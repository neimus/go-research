package without_pointer

type IncludeStruct struct {
	fieldA string
	fieldB string
	fieldC int
	fieldD bool
}

type TestStruct struct {
	fieldA string
	fieldB string
	fieldC int
	fieldD bool
	fieldE IncludeStruct
	fieldF map[string]string
	fieldG [][]int
	fieldH []int
	fieldK bool
	fieldL string
	fieldM map[string]string
	fieldN interface{}
	fieldO string
	fieldP int
	fieldR IncludeStruct
	fieldS bool
	fieldT string
}

func (t TestStruct) Sum(a, b, c string, d, e int) (int, error) {
	var count = t.fieldC + d + e + t.fieldP + t.fieldE.fieldC
	for _, gValue := range t.fieldG {
		for _, value := range gValue {
			count += value
		}
	}
	for _, value := range t.fieldH {
		count += value
	}

	var length = len(t.fieldA) + len(t.fieldB) + len(t.fieldL) + len(t.fieldO) + len(t.fieldT)
	for _, value := range t.fieldF {
		length += len(value)
	}
	for _, value := range t.fieldM {
		length += len(value)
	}

	length += len(t.fieldR.fieldB) + len(t.fieldR.fieldB) + len(a) + len(b) + len(c)

	return count + length, nil
}

func NewTestStruct() TestStruct {
	return TestStruct{
		fieldA: "Test",
		fieldB: "struct",
		fieldC: 100,
		fieldD: true,
		fieldE: IncludeStruct{
			fieldA: "include",
			fieldB: "field",
			fieldC: 200,
			fieldD: true,
		},
		fieldF: map[string]string{
			"data1":  "valueA",
			"data2":  "valueA",
			"data3":  "valueA",
			"data4":  "valueA",
			"data5":  "valueA",
			"data6":  "valueA",
			"data7":  "valueA",
			"data8":  "valueA",
			"data9":  "valueA",
			"data10": "valueA",
			"data11": "valueA",
			"data12": "valueA",
			"data14": "valueA",
			"data15": "valueA",
			"data16": "valueA",
			"data17": "valueA",
			"data18": "valueA",
			"data19": "valueA",
			"data20": "valueA",
		},
		fieldG: [][]int{
			{
				1, 2, 5, 555, 34, 12, 33, 33,
			},
			{
				1, 2, 5, 555, 34, 12, 33, 33,
			},
			{
				1, 2, 5, 555, 34, 12, 33, 33,
			},
			{
				1, 2, 5, 555, 34, 12, 33, 33,
			},
			{
				1, 2, 5, 555, 34, 12, 33, 33,
			},
			{
				1, 2, 5, 555, 34, 12, 33, 33,
			},
			{
				1, 2, 5, 555, 34, 12, 33, 33,
			},
			{
				1, 2, 5, 555, 34, 12, 33, 33,
			},
		},
		fieldH: []int{
			1, 2, 5, 555, 34, 12, 33, 33, 222, 55,
		},
		fieldK: false,
		fieldL: "ABCDEFGH1234567890",
		fieldM: map[string]string{
			"data1":  "valueA",
			"data2":  "valueA",
			"data3":  "valueA",
			"data4":  "valueA",
			"data5":  "valueA",
			"data6":  "valueA",
			"data7":  "valueA",
			"data8":  "valueA",
			"data9":  "valueA",
			"data10": "valueA",
			"data11": "valueA",
			"data12": "valueA",
			"data14": "valueA",
			"data15": "valueA",
			"data16": "valueA",
			"data17": "valueA",
			"data18": "valueA",
			"data19": "valueA",
			"data20": "valueA",
		},
		fieldN: "interface",
		fieldO: "",
		fieldP: 120340234,
		fieldR: IncludeStruct{
			fieldA: "include",
			fieldB: "second",
			fieldC: 333,
			fieldD: false,
		},
		fieldS: false,
		fieldT: "end",
	}
}
