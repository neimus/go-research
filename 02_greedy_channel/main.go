package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	checkerWriter := make([]time.Time, 30)
	checkerReader := make([]time.Time, 30)
	chTest := make(chan int, 10)

	var wg sync.WaitGroup

	// заполняем канал
	for i := 0; i < 10; i++ {
		chTest <- i
		checkerWriter[i] = time.Now()
	}

	// запускаем горутины на запись в канал
	go func() {
		for i := 10; i < 30; i++ {
			chTest <- i
			checkerWriter[i] = time.Now()
		}
		close(chTest)
	}()

	// запускаем чтение из канала
	wg.Add(30)
	go func() {
		<-time.After(1 * time.Second)
		for j := range chTest {
			// при добавлении небольшой задержки, жадное чтение/запись исчезает
			// <-time.After(1 * time.Millisecond)
			checkerReader[j] = time.Now()
		}
		wg.Add(-30)
	}()

	wg.Wait()

	OutputSortedReport(checkerWriter, checkerReader)
}

func OutputSortedReport(checkerWriter []time.Time, checkerReader []time.Time) {
	var (
		wIndex int
		rIndex int
	)
	for {
		if wIndex > 29 && rIndex > 29 {
			return
		}

		if wIndex > 29 {
			Print("read", checkerReader[rIndex], rIndex)
			rIndex++
			continue
		}

		if rIndex > 29 {
			Print("write", checkerWriter[rIndex], wIndex)
			wIndex++
			continue
		}

		if checkerWriter[wIndex].Before(checkerReader[rIndex]) {
			Print("write", checkerWriter[rIndex], wIndex)
			wIndex++
		} else {
			Print("read", checkerReader[rIndex], rIndex)
			rIndex++
		}
	}
}

func Print(name string, date time.Time, index int) {
	fmt.Printf("%d:%d %s => %d\n", date.Second(), date.Nanosecond(), name, index)
}
