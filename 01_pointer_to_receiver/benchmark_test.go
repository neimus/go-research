package main

import (
	"testing"

	"01_pointer_to_receiver/pointer"
	"01_pointer_to_receiver/without_pointer"
)

func BenchmarkTestStruct_Sum(b *testing.B) {
	st := without_pointer.NewTestStruct()
	for i := 0; i < b.N; i++ {
		_, _ = st.Sum("test", "benchmark", "struct", 12345, 5622)
	}
}

func BenchmarkNewTestStructWithPointer(b *testing.B) {
	st := pointer.NewTestStructWithPointer()
	for i := 0; i < b.N; i++ {
		_, _ = st.Sum("test", "benchmark", "struct", 12345, 5622)
	}
}
