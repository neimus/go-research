package main

import (
	"01_pointer_to_receiver/pointer"
	"01_pointer_to_receiver/without_pointer"
)

func main() {
	st := without_pointer.NewTestStruct()
	_, _ = st.Sum("test", "benchmark", "struct", 12345, 5622)

	st2 := pointer.NewTestStructWithPointer()
	_, _ = st2.Sum("test", "benchmark", "struct", 12345, 5622)
}
