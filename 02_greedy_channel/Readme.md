# Жадное чтение и запись в канал

[[_TOC_]]

## Описание
Данным исследованием хочется понять, как работает жадное чтение и запись в канал.
Для этого создадим канал `int` размерностью 10. WaitGroup будем контролировать завершения работы приложения.
Для записи времени в какой момент произошло чтение или запись используем слайсы. 
Для чистоты исследования не будет осуществлять вывод в консоль в момент чтения или записи 

Сперва полностью заполним канал (запишем 10 элементов до 9 индекса включительно), 
далее запустим горутины, которая будет писать в канал, 
после этого запускаем чтение из канала с задержкой.

## Запуск
После запуска явно видно жадное чтение и запись
```sh
39:538601000 write => 0
39:538601000 write => 1
39:538601000 write => 2
39:538601000 write => 3
39:538601000 write => 4
39:538601000 write => 5
39:538601000 write => 6
39:538601000 write => 7
39:538601000 write => 8
39:538601000 write => 9
41:539717000 read => 0
41:539718000 read => 1
41:539718000 read => 2
41:539718000 read => 3
41:539718000 read => 4
41:539718000 read => 5
41:539719000 read => 6
41:539719000 read => 7
41:539719000 read => 8
41:539719000 read => 9
41:539719000 read => 10
41:539723000 write => 10
41:539723000 write => 11
41:539723000 write => 12
41:539723000 write => 13
41:539723000 write => 14
41:539723000 write => 15
41:539723000 write => 16
41:539723000 write => 17
41:539723000 write => 18
41:539723000 write => 19
41:539723000 write => 20
41:539723000 write => 21
41:539726000 read => 11
41:539726000 read => 12
41:539726000 read => 13
41:539726000 read => 14
41:539726000 read => 15
41:539726000 read => 16
41:539727000 read => 17
41:539727000 read => 18
41:539727000 read => 19
41:539727000 read => 20
41:539727000 read => 21
41:539727000 read => 22
41:539728000 write => 22
41:539728000 write => 23
41:539728000 write => 24
41:539728000 write => 25
41:539728000 write => 26
41:539728000 write => 27
41:539728000 write => 28
41:539728000 write => 29
41:539737000 read => 23
41:539737000 read => 24
41:539737000 read => 25
41:539737000 read => 26
41:539738000 read => 27
41:539738000 read => 28
41:539738000 read => 29
```

Так же интересной особенностью является чтение и запись 10-го индекса (11-м по счету) 
Можно увидеть как работает другая оптимизация, 
которая при пустом канале значение передает сразу приемнику избегая буфера

```sh
41:539719000 read => 10
41:539723000 write => 10
```

Если добавить при чтении небольшую задержку, например в 1 миллисекунду,
то результат будет другим и "жадное чтение/запись" мы не увидим.
```sh
37:846613000 write => 0
37:846613000 write => 1
37:846613000 write => 2
37:846613000 write => 3
37:846613000 write => 4
37:846613000 write => 5
37:846613000 write => 6
37:846613000 write => 7
37:846613000 write => 8
37:846613000 write => 9
37:846613000 write => 10
38:848878000 read => 0
37:846613000 write => 11
38:850040000 read => 1
37:846613000 write => 12
38:851191000 read => 2
37:846613000 write => 13
38:852342000 read => 3
37:846613000 write => 14
38:853497000 read => 4
37:846613000 write => 15
38:854955000 read => 5
37:846613000 write => 16
38:856104000 read => 6
37:846613000 write => 17
38:857253000 read => 7
37:846613000 write => 18
38:858402000 read => 8
37:846613000 write => 19
38:860275000 read => 9
38:847787000 write => 20
38:861429000 read => 10
38:848884000 write => 21
38:862804000 read => 11
38:850045000 write => 22
38:863952000 read => 12
38:851195000 write => 23
38:865122000 read => 13
38:852345000 write => 24
38:866321000 read => 14
38:853501000 write => 25
38:867479000 read => 15
38:854958000 write => 26
38:868628000 read => 16
38:856108000 write => 27
38:869753000 read => 17
38:857256000 write => 28
38:871121000 read => 18
38:858405000 write => 29
38:872320000 read => 19
38:873472000 read => 20
38:874628000 read => 21
38:875984000 read => 22
38:877093000 read => 23
38:878232000 read => 24
38:879483000 read => 25
38:880758000 read => 26
38:881903000 read => 27
38:886028000 read => 28
38:888445000 read => 29
```

## Запуск с debugChan
Для того чтобы убедиться окончательно, что в golang есть жадное чтение, давайте посмотрим на исходный код каналов.
Можно запустить эту же программу изменив константу `debugChan` в исходниках `src/runtime/chan.go` на `true` (не забудьте вернуть обратно)
В выводе будет несколько каналов, которые создаются даже при пустой программе (Garbage collection и memory)
Нам нужен третий:
```sh
# Создаем канал
makechan: chan=0x140000b0000; elemsize=8; dataqsiz=10

# Записываем 
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000

chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000

chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000

chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000

chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000
chansend: chan=0x140000b0000

chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
chanrecv: chan=0x140000b0000
```
`makechan` - создание канала
`chansend` - запись в канал
`chanrecv` - чтение из канала


## Анлиз
Чтобы понять, почему и как работает жадное чтение, необходимо погрузиться в исходный код метода `chanrecv`
- при первом чтении, мы не обнаруживаем ждущих отправителей (sender записал все в буфер) 
Тогда мы блокируем горутину на текущем канале на определенный период времени
```go
// no sender available: block on this channel.
gp := getg()
...
// Signal to anyone trying to shrink our stack that we're about
// to park on a channel. The window between when this G's status
// changes and when we set gp.activeStackChans is not safe for
// stack shrinking.
atomic.Store8(&gp.parkingOnChan, 1)
gopark(chanparkcommit, unsafe.Pointer(&c.lock), waitReasonChanReceive, traceEvGoBlockRecv, 2)
```
- при последующих чтениях, когда время блокировки еще не вышло мы проверяем не пустой ли буфер и вычитываем данные из буфера
```go
if c.qcount > 0 {
	...
    return true, true
}
```
- далее либо время блокировки горутины на чтения истечет, либо мы вычитаем все из буфера, что приведет к блокировке горутины на чтения

В исходном коде также есть та самая оптимизация `sendDirect`,
```go
if sg.elem != nil {
    sendDirect(c.elemtype, sg, ep)
    sg.elem = nil
}
```
которая позволяет осуществить отправку данных напрямую от получателя к приемнику миную буфер

## Используемые статьи и доклады
- раздел `Buffer size or channel capacity` на сайте https://medium.com/rungo/anatomy-of-channels-in-go-concurrency-in-go-1ec336086adb
- про флаг `debugChan` https://youtu.be/Tp5xhTMFuLU?t=1053 и не только

## Интересные статьи
- https://go101.org/article/channel.html
- https://go101.org/article/channel-use-cases.html
- https://go101.org/article/channel-closing.html